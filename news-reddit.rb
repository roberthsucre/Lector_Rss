require 'rest-client'
require 'json'
require 'date'

class Show_reddit
  attr_accessor :articulos, :file, :news_array

  def initialize
    @articulos = RestClient.get('https://www.reddit.com/.json')
    @file = JSON.parse(@articulos.body)
  end

  def impresionr(file)
    file['data']['children'].each do |index|
      fecha = Time.at(index['data']['created'].to_i).strftime("%d/%m/%Y")
      puts "Titulo: #{index['data']['title']}"
      puts "Autor: #{index['data']['author']}"
      puts "Fecha: #{fecha}"
      puts "Link: #{index['data']['url']}"
      puts
    end
  end

  def save_datar(file, array)
    file['data']['children'].each do |index|
      fecha = Time.at(index['data']['created'].to_i).to_s.split(' ')
      fecha = Date.parse(fecha[0])
      @news_data ={ 'title' => "#{index['data']['title']}", 'author' => "#{index['data']['author']}", 'date' => "#{fecha.mday}/#{fecha.mon}/#{fecha.year}", 'link' => "#{index['data']['url']}"}
      array << @news_data
    end
  end

end



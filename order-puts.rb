module OrderPuts

  def self.order(array)
    array = array.sort_by {|index| index['date']}
  end

  def self.show_news(array)
    array.each do |index|
      puts "Titulo: #{index['title']}"
      puts "Autor: #{index['author']}"
      puts "Fecha: #{index['date']}"
      puts "Link: #{index['link']}"
      puts
    end
  end

end

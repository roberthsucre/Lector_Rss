require 'rest-client'
require 'json'
require 'date'

class Show_digg
  attr_accessor :articulos, :file, :news_array

  def initialize
    @articulos = RestClient.get('http://digg.com/api/news/popular.json')
    @file = JSON.parse(@articulos.body)
  end

  def impresiond(file)
    file['data']['feed'].each do |index|
      fecha = Time.at(index['date'].to_i).to_s.split(' ')
      fecha = Date.parse(fecha[0])
      puts "Titulo: #{index['content']['title_alt']}"
      if index['content']['author'] == ""
        puts 'Autor: No tenemos nombre del autor'
      else
        puts "Autor: #{index['content']['author']}"
      end
      puts "Fecha #{fecha.mday}/#{fecha.mon}/#{fecha.year}"
      puts "Link: #{index['content']['original_url']}"
      puts
    end
  end

  def save_datad(file, array)
    file['data']['feed'].each do |index|
      fecha = Time.at(index['date'].to_i).to_s.split(' ')
      fecha = Date.parse(fecha[0])
      @news_data ={ 'title' => "#{index['content']['title_alt']}", 'author' => "#{index['content']['author']}", 'date' => "#{fecha.mday}/#{fecha.mon}/#{fecha.year}", 'link' => "#{index['content']['original_url']}"}
      if @news_data['author'] == ""
        @news_data['author'] = 'No tenemos nombre del autor'
      end
      array << @news_data
    end
  end
end


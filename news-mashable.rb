require 'rest-client'
require 'json'
require 'date'

class Show_mashable
  attr_accessor :articulos, :file, :news_array

  def initialize
    @articulos = RestClient.get('http://mashable.com/stories.json')
    @file = JSON.parse(@articulos.body)
  end

  def impresionm(file)
    file.each do |index, value|
      if index != 'channel'
        puts "    #{index.upcase}"
        file[index].each do |index|
          fecha = Date.parse(index['post_date'])
          puts "Titulo #{index['title']}"
          puts "Autor #{index['author']}"
          puts "Fecha #{fecha.mday}/#{fecha.mon}/#{fecha.year}"
          puts "Link #{index['link']}"
          puts
        end
      end
    end
  end

  def save_datam(file, array)
    file.each do |index, value|
      if index != 'channel'
        file[index].each do |index|
          fecha = Date.parse(index['post_date'])
          @news_data ={ 'title' => "#{index['title']}", 'author' => "#{index['author']}", 'date' => "#{fecha.mday}/#{fecha.mon}/#{fecha.year}", 'link' => "#{index['link']}"}
          array << @news_data
        end
      end
    end
  end

end

